# -*- coding: utf-8 -*-

# USAGE
# python ball_tracking.py --video ball_tracking_example.mp4
# python ball_tracking.py

# importowanie potrzebnych pakietów
from collections import deque
from imutils.video import VideoStream
import numpy as np
import argparse
import cv2
import imutils
import time

# parsowanie argumentów z linii komend:
# -v żródło video, może być plik
# -b długość bufora, domyślnie 64
ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video",
	help="path to the (optional) video file")
ap.add_argument("-b", "--buffer", type=int, default=64,
	help="max buffer size")
args = vars(ap.parse_args())

# definicja dolnej i górnej 'granicy' koloru zielonego, który będziemy śledzić
greenLower = (35, 86, 70)
greenUpper = (60, 255, 255)

# inicjalizacja kolejki w których będą przechowywane punkty wyśledzone na obrazie
pts = deque(maxlen=args["buffer"])

# inicjalizacja kolejek dla współrzędnych x i y śledzonych punktów
# wykorzystywanych później do sprawdzenia ruchu w pionie i poziomie
pts_x = deque(maxlen=args["buffer"])
pts_y = deque(maxlen=args["buffer"])

# jeżeli nie podano pliku wideo, uruchamiamy kamerę:
if not args.get("video", False):
	vs = VideoStream(src=0).start()

# jeżeli podano plik, otwieramy plik:
else:
	vs = cv2.VideoCapture(args["video"])

# 2 sekundy oczekiwania na uruchomienie kamery lub wczytanie pliku
time.sleep(2.0)

# Definija czcionki napisów wyświetlanych na ekranie
font = cv2.FONT_HERSHEY_SIMPLEX

# Główna pętla programu, trwa do końca pliku wideo
# lub do przerwania poprzez wciśnięcie q.
while True:
	# pobierz klatkę obrazu
	frame = vs.read()

	# handle the frame from VideoCapture or VideoStream
	frame = frame[1] if args.get("video", False) else frame

	# Jeżeli pobieramy klatki z pliku, i pobraliśmy pustą klatkę,
	# oznacza to, że dotarliśmy do końca pliku.
	if frame is None:
		break

	# Zmiana rozmiaru klatki do szerokości 600px.color space
	frame_orig = imutils.resize(frame, width=600)
	# Przerzucenie klatki w poziomie (efekt lustrzany)
	frame = cv2.flip(frame_orig, 1)
	# Rozmazanie klatki
	blurred = cv2.GaussianBlur(frame, (11, 11), 0)
	# Przetworzenie klatki do reprezentacji obrazu w postaci HSV
	hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

	# Stworzenie maski dla koloru zielonego (wycięcie z obrazu
	# części zielonych), a następnie przeprowadzenie 2 iterac
	# operacji erozji i dylatacji w celu usunięcia mniejszych
	# plam pozostałych w masce.
	mask = cv2.inRange(hsv, greenLower, greenUpper)
	mask = cv2.erode(mask, None, iterations=2)
	mask = cv2.dilate(mask, None, iterations=2)

	# Odnalezienie konturów w masce i inicjalizacja obecnego
	# środka piłki (center)
	cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
	cnts = imutils.grab_contours(cnts)
	center = None

	# Inicjalizacja również punktów środka (x,y)
	x = None
	y = None

	# Kontynuujemy wyłącznie jeżeli odnaleziono chociaż jeden kontur:
	if len(cnts) > 0:
		# Znalezienie największego konturu w masce, a następnie
		# obliczenie najmniejszego okręgu który go obejmie.
		c = max(cnts, key=cv2.contourArea)
		((x, y), radius) = cv2.minEnclosingCircle(c)
		M = cv2.moments(c)
		center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

		# Obliczenie współrzędnych x i y środka.
		x = int(M["m10"] / M["m00"])
		y = int(M["m01"] / M["m00"])

		# Kontynuuj tylko jeżeli promień okręgu jest większy niż 10
		if radius > 10:
			# Wyrysowanie okręgu i centroidu
			cv2.circle(frame, (int(x), int(y)), int(radius),
				(0, 255, 255), 2)
			cv2.circle(frame, center, 5, (0, 0, 255), -1)

	# Dodanie punktu do kolejki
	pts.appendleft(center)

	# Jeżeli x i y nie były puste, dodanie również do kolejki (są puste, jeżeli nie pojawia się nic zielonego)
	if x != None:
		pts_x.appendleft(x)
	if y != None:
		pts_y.appendleft(y)

	# Wypisanie na ekran współrzędnych x i y środka zielonego obiektu.
	cv2.putText(frame, str(x)+":"+str(y), (0,50), font, 1, (255,255,255), 2, cv2.CV_AA)

	# Jeżeli kolejki współrzędnych x i y nie są puste:
	if pts_x and pts_y:
		# Znalezienie minimalnej i maksymalnej wartości wsp. x w kolejce
		min_x = min(pts_x)
		max_x = max(pts_x)
		# Obliczenie różnicy pomiędzy maksymalną i minimalną wartością wsp. x w kolejce
		diff_x = max_x - min_x
		# Wypisanie  minimalnej i maksymalnej wartości wsp. x w kolejce
		cv2.putText(frame, "min_x:"+str(min_x), (0,75), font, 1, (255,255,255), 2, cv2.CV_AA)
		cv2.putText(frame, "max_x:"+str(max_x), (0,100), font, 1, (255,255,255), 2, cv2.CV_AA)

		# Znalezienie minimalnej i maksymalnej wartości wsp. x w kolejce
		min_y = min(pts_y)
		max_y = max(pts_y)
		# Obliczenie różnicy pomiędzy maksymalną i minimalną wartością wsp. y w kolejce
		diff_y = max_y - min_y
		# Wypisanie  minimalnej i maksymalnej wartości wsp. y w kolejce
		cv2.putText(frame, "min_y:"+str(min_y), (0,125), font, 1, (255,255,255), 2, cv2.CV_AA)
		cv2.putText(frame, "max_y:"+str(max_y), (0,150), font, 1, (255,255,255), 2, cv2.CV_AA)

		# Jeżeli różnica maksymalnych i minimalnych wartości wsp. x i y jest mniejsza niż 100 px
		# oznacza to, że mamy brak ruchu (albo bardzo mały ruch)
		if (diff_x < 100) and (diff_y<100):
			cv2.putText(frame, "bez ruchu", (200,85), font, 1, (255,255,255), 2, cv2.CV_AA)

		# Jeżeli tylko różnica wsp. x jest mała, to mamy ruch w pionie
		elif (diff_x < 100):
			cv2.putText(frame, "ruch w pionie", (200,85), font, 1, (255,255,255), 2, cv2.CV_AA)

		# Jeżeli tylko różnica wsp. y jest mała, to mamy ruch w poziomie
		elif (diff_y < 100):
			cv2.putText(frame, "ruch w poziomie", (200,85), font, 1, (255,255,255), 2, cv2.CV_AA)


	# Pętla przez wszystkie punkty w kolejce
	for i in range(1, len(pts)):
		# Punkty w kolejce, które mają wartość None (puste) są pomijane
		if pts[i - 1] is None or pts[i] is None:
			continue

		# Oblczienie linii pomiędzy kolejnymi punktami i jej grubości
		thickness = int(np.sqrt(args["buffer"] / float(i + 1)) * 2.5)
		cv2.line(frame, pts[i - 1], pts[i], (0, 0, 255), thickness)

	# Wyświetlenie klatki obrazu na ekranie.
	cv2.imshow("Frame", frame)
	key = cv2.waitKey(1) & 0xFF

	# Przerwanie głównej pętli programu jeżeli wciśnięto 'q'.
	if key == ord("q"):
		break

# Jeżeli nie używaliśmy pliku wideo, a czytaliśmy obraz z kamery, należy ją zwolnić
if not args.get("video", False):
	vs.stop()
	vs.stream.release()

# w przeciwnym wypadku, zamykamy plik wideo
else:
	vs.release()

# Zamknięcie otwartego okna
cv2.destroyAllWindows()
